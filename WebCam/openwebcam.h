#ifndef OPENWEBCAM_H
#define OPENWEBCAM_H

#include <QDialog>

namespace Ui {
class OpenWebcam;
}
class QCamera;
class QCameraViewfinder;
class QCameraImageCapture;
class QVBoxLayout;
class QMenu;
class QAction;

class OpenWebcam : public QDialog
{
    Q_OBJECT

public:
    explicit OpenWebcam(QWidget *parent = nullptr);
    ~OpenWebcam();

private:
    Ui::OpenWebcam *ui;
    QCamera *mCamera;
    QCameraViewfinder *mCameraViewfinder;
    QCameraImageCapture *mCameraImageCapture;
    QVBoxLayout *mLayout;
    QMenu *mOpenMenu;
    QAction *mEncenderAction;
    QAction *mApagarAction;
    QAction *mCapturarAction;
};

#endif // OPENWEBCAM_H
