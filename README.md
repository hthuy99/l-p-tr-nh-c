<h1>LẬP TRÌNH GIAO DIỆN BẰNG QT</h1>
<h1>1. Định hướng và Các kết quả then chốt cần đạt được </h1>
<h2>1.1 Định hướng</h2>
<ul>
  <li>Học tập môn lập trình C </li>
  <li>Học làm teamwork</li>
  <li>Học Gitlab</li>
  <li>Học hỏi, trau dồi code C </li>
  <li>Học hỏi cách code giao diện trong QT bằng ngôn ngữ C</li>
</ul>
<h2>1.2 Các kết quả then chốt cần đạt được</h2>
<ul>
  <li>Tên đề tài: LẬP TRÌNH GIAO DIỆN BẰNG QT</li>
  <li>Thiết kế được giao diện QT một cách đúng đắn, hiệu quả</li>
</ul>

<h1>2. Công nghệ và công cụ </h1>
  <ul>
  <li>Ngôn ngữ C</li>
  <li>Công cụ QT</li>
  </ul>

<h1>3. Hướng dẫn sử dụng và cài đặt</h1>
<h1>4. Thành viên trong nhóm </h1>
  <ul>
  <li>Trần Ngọc Bích </li>
  <li>Nguyễn Thị Thắm </li>
  <li>Bùi Thị Hồng Thúy </li>
  <li>Vũ Thị Hạnh Trang</li>
  <li>Mai Huyền Trang</li>
  </ul

