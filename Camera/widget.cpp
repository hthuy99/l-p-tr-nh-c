#include "widget.h"
#include "ui_widget.h"
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraImageCapture>
#include <QVBoxLayout>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    mCamera = new QCamera(this);
    mCameraViewfinder = new QCameraViewfinder(this);
    mCameraImageCapture = new QCameraImageCapture(mCamera, this);
    mLayout = new QVBoxLayout;
    mOpenMenu = new QMenu("Open", this);
    mEncenderAction = new QAction("Encender", this);
    mApagarAction = new QAction("Stop", this);
    mCapturarAction = new QAction("Capturar", this);

    mOpenMenu->addActions({mEncenderAction,mApagarAction, mCapturarAction});

    ui->open->setMenu(mOpenMenu);
    mCamera->setViewfinder(mCameraViewfinder);
    mLayout->addWidget(mCameraViewfinder);
    mLayout->setMargin(0);
    ui->scrollArea->setLayout(mLayout);

    connect(mEncenderAction, &QAction::triggered, [&](){
        mCamera->start();
    });
    connect(mApagarAction, &QAction::triggered, [&](){
        mCamera->stop();
    });
    connect(mCapturarAction, &QAction::triggered, [&](){
        auto filename = QFileDialog::getSaveFileName(this, "Captural","/","Imagen(*.jpg;*.jpeg)");
        mCameraImageCapture->setCaptureDestination(
                    QCameraImageCapture::CaptureToFile);
        QImageEncoderSettings imageEncoderSettings;
        imageEncoderSettings.setCodec("image/jpeg");
        imageEncoderSettings.setResolution(1600, 1200);
        mCameraImageCapture->setEncodingSettings(imageEncoderSettings);
        mCamera->setCaptureMode(QCamera::CaptureStillImage);
        mCamera->start();
        mCamera->searchAndLock();
        mCameraImageCapture->capture(filename);
        mCamera->unlock();
    });
}

Widget::~Widget()
{
    delete ui;
}

